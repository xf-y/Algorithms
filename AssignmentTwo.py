import AssignmentTwoTools as a2Tools

# SSL
# ssl_dataframe = a2Tools.value_split('SSL', '\s+', 'square', 'suffix')
# a2Tools.nominal_statistic('square', ssl_dataframe)
# a2Tools.nominal_statistic('suffix', ssl_dataframe)

# BATHRM
# a2Tools.ratio_statistic('BATHRM', True)
# a2Tools.three_sigma_outliers_detection('BATHRM', False)


# HF_BATHRM
# a2Tools.ratio_statistic('HF_BATHRM', False)
# a2Tools.three_sigma_outliers_detection('HF_BATHRM', False)

# HEAT
# a2Tools.nominal_statistic('HEAT')

# HEAT_D
# a2Tools.nominal_statistic('HEAT_D')

# AC
# a2Tools.nominal_statistic('AC')

# NUM_UNITS
# a2Tools.ratio_statistic('NUM_UNITS', False)
# a2Tools.three_sigma_outliers_detection('NUM_UNITS', False)

# ROOMS
# a2Tools.ratio_statistic('ROOMS', False)
# a2Tools.three_sigma_outliers_detection('ROOMS', False)

# BEDRM
# a2Tools.ratio_statistic('BEDRM', False)
# a2Tools.three_sigma_outliers_detection('BEDRM', True)

# AYB
# a2Tools.inverval_statistic('AYB', True)

# YR_RMDL
# a2Tools.inverval_statistic('YR_RMDL', True)

# EYB
# a2Tools.inverval_statistic('EYB', True)

# STORIES
# a2Tools.ratio_statistic('STORIES', False)
# a2Tools.three_sigma_outliers_detection('STORIES', True)

# SALEDATE
# saledate_dataframe = a2Tools.value_split('SALEDATE', 'T', 'SALEDATE', 'suffix')
# a2Tools.inverval_statistic('SALEDATE', False, saledate_dataframe)

# PRICE
# a2Tools.ratio_statistic('PRICE', True)
# a2Tools.three_sigma_outliers_detection('PRICE', True)

# QUALIFIED
# a2Tools.nominal_statistic('QUALIFIED')

# SALE_NUM
# a2Tools.ratio_statistic('SALE_NUM', False)
# a2Tools.three_sigma_outliers_detection('SALE_NUM', False)

# GBA
# a2Tools.ratio_statistic('GBA', True)
# a2Tools.three_sigma_outliers_detection('GBA', True)

# BLDG_NUM
# a2Tools.ratio_statistic('BLDG_NUM', True)

# STYLE
# a2Tools.nominal_statistic('STYLE')

# STYLE_D
# a2Tools.nominal_statistic('STYLE_D')

# STRUCT
# a2Tools.nominal_statistic('STRUCT')

# STRUCT_D
# a2Tools.nominal_statistic('STRUCT_D')

# GRADE
# a2Tools.nominal_statistic('GRADE')

# GRADE_D
# a2Tools.nominal_statistic('GRADE_D')

# equal-width bin


# equal-depth & equal-depth bin
# a2Tools.equal_width_bin('PRICE', 10)
# a2Tools.equal_depth_bin('PRICE', 95)

# min-max normalization
# a2Tools.min_max_normalization('PRICE')

# z-score normalization
# a2Tools.z_score_normalization('PRICE')

# find clusters
# a2Tools.find_cluster()

# price category
# a2Tools.price_statistic('PRICE')

# binarize struct_d
# a2Tools.binarize_column('STRUCT_D')

# find two-dimensional clusters
# a2Tools.dbscan_clusters_detection('AYB', 'YR_RMDL', eps=0.2)

# find three-dimensional clusters
# a2Tools.three_dimensional_cluster_detection('AYB', 'EYB', 'YR_RMDL')

