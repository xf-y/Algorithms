from sklearn.ensemble import GradientBoostingClassifier
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split, GridSearchCV
import pandas as pd
import numpy as np
from imblearn.over_sampling import SMOTE
import AssignmentThreeTools as a3Tools


def optimize_boosting_parameters(input_data, output_data):
    gradient_boost_parameters = {
        'n_estimators': np.arange(800, 900, 5)
    }
    optimized_classifier = GridSearchCV(GradientBoostingClassifier(n_estimators=1000), gradient_boost_parameters, scoring='f1', cv=5, n_jobs=4)
    optimized_classifier.fit(input_data, output_data)
    print(optimized_classifier.best_params_, optimized_classifier.best_score_)


data_input, data_output = a3Tools.get_train_data()
# optimize_boosting_parameters(data_input, data_output)
while True:
    train_input, test_input, train_output, test_output = train_test_split(data_input, data_output, train_size=0.7, test_size=0.3)
    train_input_smote, train_output_smote = SMOTE(random_state=2).fit_sample(train_input, train_output)
    gradient_boost_classifier = GradientBoostingClassifier(n_estimators=860, warm_start=True)
    gradient_boost_classifier.fit(train_input_smote, train_output_smote)
    predict_test_output = gradient_boost_classifier.predict(test_input)
    predict_accuracy = accuracy_score(test_output, predict_test_output)
    if predict_accuracy > 0.87:
        data_test = a3Tools.get_test_data()
        data_input_rowid = data_test.loc[:, 'row ID']
        data_input_test = data_test.iloc[:, 1:]
        data_output_test = gradient_boost_classifier.predict(data_input_test)
        a3Tools.write_data(pd.DataFrame({'row ID': data_input_rowid, 'QUALIFIED': data_output_test}), 'GradientBoosting')
        break
    else:
        print(predict_accuracy)


