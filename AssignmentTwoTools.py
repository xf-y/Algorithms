import pandas
import re
import numpy
import matplotlib.pyplot
import sklearn.cluster
import plotly.plotly

plotly.tools.set_credentials_file(username='BxtenFly', api_key='u6usY5DFIFtrQFB38Ebx')
original_csv = pandas.read_csv('C:\\Users\\Vincent\\Documents\\2018 spring semester\\32130 Fundamental Data analytics\\Assignment 2\\dataset\\Xiongfei_Yu.csv')
my_csv = pandas.read_csv('C:\\Users\\Vincent\\Documents\\2018 spring semester\\32130 Fundamental Data analytics\\Assignment 2\\dataset\\Xiongfei_Yu.csv', usecols=list(range(0, 26)))


def value_split(column_name, regex, column_one_name, column_two_name, dataresource=my_csv):
    column_one = []
    column_two = []
    for value in dataresource[column_name]:
        column_one.append(re.split(regex, value)[0])
        column_two.append(re.split(regex, value)[1])
    processed_dataframe = pandas.DataFrame(data={column_one_name: column_one, column_two_name: column_two})
    return processed_dataframe


def ratio_statistic(column_name, omit_zero, data_resource=my_csv):
    if omit_zero:
        count_statistic = data_resource[column_name][~numpy.isnan(data_resource[column_name])][data_resource[column_name] != 0]
    else:
        count_statistic = data_resource[column_name][~numpy.isnan(data_resource[column_name])]
    column_value = count_statistic.sort_values().unique()
    column_min = count_statistic.min()
    column_max = count_statistic.max()
    column_mean = count_statistic.mean()
    column_median = count_statistic.median()
    column_variance = count_statistic.var()
    column_standard_deviation = count_statistic.std()
    column_quantile = pandas.DataFrame(count_statistic).quantile([0.25, 0.5, 0.75, 1])
    statistic_statement = '=========================================\n' \
                          '%s statistics are: \n' \
                          'range: %d(min) - %d(max)\n' \
                          'mean: %.2f\n' \
                          'value: %s\n' \
                          'median: %.2f\n' \
                          'variance: %.2f\n' \
                          'standard deviation: %.2f\n' \
                          'quantile [0.25, 0.5, 0.75, 1]: %s\n' \
                          '=========================================' \
                          % (column_name, column_min, column_max, column_mean, column_value, column_median, column_variance, column_standard_deviation, column_quantile)
    print(statistic_statement)

    figure_box = matplotlib.pyplot.figure(1)
    matplotlib.pyplot.boxplot(count_statistic, meanline=True, showmeans=True)
    figure_box.show()

    count_frequency = count_statistic.value_counts().sort_index()
    binning_frequency = pandas.Series()
    frequency_object = count_frequency

    if len(count_frequency.index) > 100:
        for i in range(0, len(count_frequency.index), 100):
            xaxis_item = count_frequency.index[i: i + 100]
            xaxis_item_name = str(xaxis_item.values[0]) + '-\n' + str(xaxis_item.values[int(len(xaxis_item) - 1)])
            yaxis_item = sum(count_frequency[xaxis_item])
            binning_frequency[str(xaxis_item_name)] = yaxis_item
        frequency_object = binning_frequency

    frequenct_statement = '=========================================\n' \
                          '%s frequency:\n' \
                          '%s\n' \
                          '=========================================' \
                          % (column_name, frequency_object)
    print(frequenct_statement)

    figure_bar = matplotlib.pyplot.figure(2)
    matplotlib.pyplot.xlabel(column_name)
    matplotlib.pyplot.ylabel('Frequency')
    # matplotlib.pyplot.xticks(range(21), range(21))
    figure_ratio = matplotlib.pyplot.bar(frequency_object.index, frequency_object.values)
    for rect in figure_ratio:
        rect_height = rect.get_height()
        matplotlib.pyplot.text(rect.get_x() + rect.get_width() / 2.0, rect_height, '%d' % int(rect_height), ha='center', va='bottom')
    figure_bar.show()


def nominal_statistic(column_name, data_resource=my_csv):
    count_frequency = data_resource[column_name].value_counts().sort_index()
    binning_frequency = pandas.Series()
    frequency_object = count_frequency

    if len(count_frequency.index) > 20:
        for i in range(0, len(count_frequency.index), 70):
            xaxis_item = count_frequency.index[i: i + 70]
            xaxis_item_name = str(xaxis_item.values[0]) + '-' + str(xaxis_item.values[int(len(xaxis_item) - 1)])
            yaxis_item = sum(count_frequency[xaxis_item])
            binning_frequency[str(xaxis_item_name)] = yaxis_item
        frequency_object = binning_frequency

    matplotlib.pyplot.xlabel(column_name)
    matplotlib.pyplot.ylabel('Frequency')
    # matplotlib.pyplot.xticks(range(14), range(14))
    # xaxis_name = list(map(lambda x: '\n'.join(re.split('\s+|\-+', x)), frequency_object.index._data))
    figure_nominal = matplotlib.pyplot.bar(frequency_object.index, frequency_object.values)
    for rect in figure_nominal:
        rect_height = rect.get_height()
        matplotlib.pyplot.text(rect.get_x() + rect.get_width()/2.0, rect_height, '%d' % int(rect_height), ha='center', va='bottom')
    print_statement = '=========================================\n' \
                      '%s frequency:\n' \
                      '%s\n' \
                      '=========================================\n' \
                      % (column_name, frequency_object)
    print(print_statement)
    matplotlib.pyplot.show()


def inverval_statistic(column_name, just_year_number, data_resource=my_csv):
    if just_year_number:
        count_statistic = data_resource[column_name][~numpy.isnan(data_resource[column_name])][data_resource[column_name] != 0]
    else:
        processed_data = pandas.Series(pandas.to_datetime(column_value) for column_value in data_resource[column_name])
        count_statistic = processed_data[~processed_data.isnull()]
    count_frequency = count_statistic.value_counts().sort_index()
    # count_frequency['nan_or_zero'] = 2000 - sum(count_frequency.values)

    if just_year_number:
        column_min = count_statistic.min()
        column_max = count_statistic.max()
        column_mean = count_statistic.mean()
        column_median = count_statistic.median()
        print_statement = '=========================================\n' \
                          '%s statistics are:\n' \
                          'range: %d(min) - %d(max)\n' \
                          'mean: %d\n' \
                          'median: %d\n' \
                          'frequency: %s\n' \
                          '=========================================\n' \
                          % (column_name, column_min, column_max, column_mean, column_median, count_frequency)
    else:
        print_statement = '=========================================\n' \
                          '%s frequency: \n' \
                          '%s\n' \
                          '=========================================\n' \
                          % (column_name, count_frequency)
    print(print_statement)

    matplotlib_pyplot_figure = matplotlib.pyplot.figure()
    matplotlib.pyplot.xlabel(column_name)
    matplotlib.pyplot.ylabel('Frequency')
    figure_interval = matplotlib_pyplot_figure.add_subplot(111)
    figure_interval.plot(count_frequency.index.astype(str), count_frequency.values)
    for tick in figure_interval.xaxis.get_major_ticks()[::1]:
        tick.set_visible(False)
    for tick in figure_interval.xaxis.get_major_ticks()[::250]:
        tick.set_visible(True)
    matplotlib.pyplot.show()


def equal_width_bin(column_name, bin_number, data_resource=my_csv):
    valid_column = data_resource[column_name][~numpy.isnan(data_resource[column_name])][data_resource[column_name] != 0]
    column_min = valid_column.min()
    column_max = valid_column.max()
    bin_interval = (column_max - column_min) / bin_number
    bin_boundaries = numpy.arange(column_min, column_max + 1, bin_interval)
    bins_result = pandas.cut(valid_column, bin_boundaries).value_counts().sort_index()
    print(bins_result)


def equal_depth_bin(column_name, capacity, data_resource=my_csv):
    valid_column = data_resource[column_name][~numpy.isnan(data_resource[column_name])][data_resource[column_name] != 0]
    column_quantity = len(valid_column)
    bins_result = pandas.qcut(valid_column, int(numpy.ceil(column_quantity / capacity))).value_counts().sort_index()
    print(bins_result)


def min_max_normalization(column_name, data_resource=my_csv):
    valid_column = data_resource[column_name][~numpy.isnan(data_resource[column_name])][data_resource[column_name] != 0]
    column_min = valid_column.min()
    column_max = valid_column.max()
    normalized_values = [(column_value - column_min) / (column_max - column_min) for column_value in valid_column]

    missing_rows = [item for item in numpy.arange(0, 2000, 1) if item not in list(map(int, valid_column.index.values))]
    normalize_values_dataframe_part_one = pandas.DataFrame({'row_number': valid_column.index, 'normalized_data': normalized_values})
    normalize_values_dataframe_part_two = pandas.DataFrame({'row_number': missing_rows, 'normalized_data': [0] * len(missing_rows)})
    normalize_values_dataframe = pandas.concat([normalize_values_dataframe_part_one, normalize_values_dataframe_part_two]).sort_values(by=['row_number'])

    original_csv['MIN-MAX NORMALIZED PRICE'] = normalize_values_dataframe['normalized_data'].values
    original_csv.to_csv('C:\\Users\\Vincent\\Documents\\2018 spring semester\\32130 Fundamental Data analytics\\Assignment 2\\dataset\\Xiongfei_Yu.csv')


def z_score_normalization(column_name, data_resource=my_csv):
    valid_column = data_resource[column_name][~numpy.isnan(data_resource[column_name])][data_resource[column_name] != 0]
    column_mean = valid_column.mean()
    column_standard_deviation = valid_column.std()
    normalized_values = [(column_value - column_mean) / column_standard_deviation for column_value in valid_column]

    missing_rows = [item for item in numpy.arange(0, 2000, 1) if item not in list(map(int, valid_column.index.values))]
    normalize_values_dataframe_part_one = pandas.DataFrame({'row_number': valid_column.index, 'normalized_data': normalized_values})
    normalize_values_dataframe_part_two = pandas.DataFrame({'row_number': missing_rows, 'normalized_data': [0] * len(missing_rows)})
    normalize_values_dataframe = pandas.concat([normalize_values_dataframe_part_one, normalize_values_dataframe_part_two]).sort_values(by=['row_number'])

    original_csv['Z-SCORE NORMALIZED PRICE'] = normalize_values_dataframe['normalized_data'].values
    original_csv.to_csv('C:\\Users\\Vincent\\Documents\\2018 spring semester\\32130 Fundamental Data analytics\\Assignment 2\\dataset\\Xiongfei_Yu.csv')


def price_statistic(column_name, data_resource=my_csv):
    valid_column = data_resource[column_name][~numpy.isnan(data_resource[column_name])][data_resource[column_name] != 0]

    xaxis = ['0-50k', '51k-100k', '101k-1000k', '10001k+']
    yaxis_tier_low = 0
    yaxis_tier_medium = 0
    yaxis_tier_high = 0
    yaxis_tier_expensive = 0

    for i in valid_column.values:
        if 0 < i < 50000:
            yaxis_tier_low += 1
        elif 50000 < i < 100000:
            yaxis_tier_medium += 1
        elif 100000 < i < 1000000:
            yaxis_tier_high += 1
        elif 1000000 < i:
            yaxis_tier_expensive += 1
    print_statement = '=========================================\n' \
                      '%s tier frequency is shown below:\n' \
                      'low tier frequency: %d\n' \
                      'medium tier frequency: %d\n' \
                      'high tier frequency: %d\n' \
                      'expensive tier frequency: %d\n' \
                      '=========================================' \
                      % (column_name, yaxis_tier_low, yaxis_tier_medium, yaxis_tier_high, yaxis_tier_expensive)
    print(print_statement)
    figure_price_bar = matplotlib.pyplot.bar(xaxis, [yaxis_tier_low, yaxis_tier_medium, yaxis_tier_high, yaxis_tier_expensive])
    for rect in figure_price_bar:
        rect_height = rect.get_height()
        matplotlib.pyplot.text(rect.get_x() + rect.get_width() / 2.0, rect_height, '%d' % int(rect_height), ha='center', va='bottom')


def binarize_column(column_name, data_resource=my_csv):
    column_values = data_resource[column_name][~data_resource[column_name].isnull()].sort_values().unique()
    if len(column_values) % 2 == 0:
        divide_value = int((len(column_values) / 2))
        zero_values = column_values[0: divide_value]
        one_values = column_values[divide_value:]
    else:
        divide_value = int(numpy.ceil(len(column_values) / 2))
        zero_values = column_values[0: divide_value]
        one_values = column_values[divide_value:].append('empty')
    binarized_values = pandas.DataFrame({'0': zero_values, '1': one_values})
    print(binarized_values)


def three_sigma_outliers_detection(column_name, omit_zero, data_resource=my_csv):
    if omit_zero:
        valid_column = data_resource[column_name][~numpy.isnan(data_resource[column_name])][data_resource[column_name] != 0]
    else:
        valid_column = data_resource[column_name][~numpy.isnan(data_resource[column_name])]
    normalized_values = [(column_value - valid_column.mean()) / valid_column.std() for column_value in valid_column]
    column_outliers = []
    for i in normalized_values:
        if i > 3 or i < -3:
            original_value = (i * valid_column.std()) + valid_column.mean()
            column_outliers.append(int(original_value))
    outliers_dataframe = pandas.DataFrame({'outliers': column_outliers})
    print(outliers_dataframe['outliers'].sort_values().unique())


def dbscan_clusters_detection(column_name_one, column_name_two, eps=0.3, data_resource=my_csv):
    target_columns = data_resource.loc[:, [column_name_one, column_name_two]].dropna()
    ratio_columns = target_columns[target_columns[column_name_one] != 0][target_columns[column_name_two] != 0]
    xdata = ratio_columns[column_name_one]
    ydata = ratio_columns[column_name_two]
    normalized_xdata = [(column_value - xdata.mean()) / xdata.std() for column_value in xdata.values]
    normalized_ydata = [(column_value - ydata.mean()) / ydata.std() for column_value in ydata.values]
    normalized_columns = pandas.DataFrame({column_name_one: normalized_xdata, column_name_two: normalized_ydata})
    dbscan_model = sklearn.cluster.DBSCAN(eps=eps).fit_predict(normalized_columns)
    figure = matplotlib.pyplot.figure()
    ax = figure.add_axes([0.1, 0.1, 0.8, 0.8])
    ax.scatter(xdata, ydata, c=dbscan_model)
    ax.set_xlabel(column_name_one)
    ax.set_ylabel(column_name_two)
    matplotlib.pyplot.title('%s-%s Cluster' % (column_name_one, column_name_two))


def three_dimensional_cluster_detection(column_name_one, column_name_two, column_name_three, data_resource=my_csv):
    target_columns = data_resource.loc[:, [column_name_one, column_name_two, column_name_three]].dropna()
    target_columns_values = target_columns[target_columns[column_name_one] != 0][target_columns[column_name_two] != 0][target_columns[column_name_three] != 0]
    scatter = dict(
        mode='markers',
        name='y',
        type='scatter3d',
        x=target_columns_values[column_name_one], y=target_columns_values[column_name_two], z=target_columns_values[column_name_three],
        marker=dict(size=2, color="rgb(23, 190, 207)")
    )

    clusters = dict(
        alphahull=7,
        name="y",
        opacity=0.1,
        type="mesh3d",
        x=target_columns_values[column_name_one], y=target_columns_values[column_name_two], z=target_columns_values[column_name_three]
    )

    layout = dict(
        title='%s-%s-%s clustering' % (column_name_one, column_name_two, column_name_three),
        scene=dict(
            xaxis=dict(zeroline=False),
            yaxis=dict(zeroline=False),
            zaxis=dict(zeroline=False),
        )
    )

    file_name = '%s - %s - %s' % (column_name_one, column_name_two, column_name_three)
    figure = dict(data=[scatter, clusters], layout=layout)
    plotly.plotly.iplot(figure, filename=file_name)
