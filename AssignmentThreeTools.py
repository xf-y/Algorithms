import re, itertools, glob, os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import sklearn.preprocessing as preprocessor
from sklearn.metrics import roc_curve, auc
pd.options.mode.chained_assignment = None


FILE_PATH = 'C:\\Users\\Vincent\\Documents\\2018 spring semester\\32130 Fundamental Data Analytics\\Assignment 3\\'


def list_minus(list_one, list_two):
    list_one = list(list_one)
    list_two = list(list_two)
    for item in list_two:
        list_one.remove(item)
    return list_one


def tuple_to_string(tuple_entity):
    string_value = ''
    for item in tuple_entity:
        string_value += '*' + item
    return string_value[1:]


def threshold_moving(prediction_proba, threshold):
    predict_output = []
    for i in prediction_proba:
        if (1 - i[0]) > threshold:
            predict_output.append(1)
        else:
            predict_output.append(0)
    return predict_output


def get_assignment_two_data():
    assignment_two_file = pd.concat((pd.read_csv(f) for f in glob.glob(os.path.join('C:\\Users\\Vincent\\Documents\\2018 spring semester\\32130 Fundamental Data analytics\\Assignment 2\\dataset\\', '*.csv'))), ignore_index=True)
    assignment_two_file.loc[assignment_two_file.loc[:, 'QUALIFIED'] == 'U', 'QUALIFIED'] = 0
    assignment_two_file.loc[assignment_two_file.loc[:, 'QUALIFIED'] == 'Q', 'QUALIFIED'] = 1
    assignment_two_file.to_csv('Assignment_Two.csv', index=False)


# split column values
def value_split(data_resource, column_name, columns, split_regex, replace_regex, slice_number):
    column_one = []
    column_two = []
    processed_data_one = ''
    processed_data_two = ''
    for value in data_resource[column_name]:
        if split_regex:
            if re.match('\w+' + split_regex + '\w+', value):
                processed_data_one = re.split(split_regex, value)[0]
                processed_data_two = re.split(split_regex, value)[1]
            else:
                processed_data_one = value
                processed_data_two = '0'
        if replace_regex:
            processed_data_one = processed_data_one.replace(replace_regex[0], replace_regex[1])
            processed_data_two = processed_data_two.replace(replace_regex[0], replace_regex[1])
        if slice_number:
            processed_data_one = processed_data_one[slice_number[0]: slice_number[1]]
            processed_data_two = processed_data_two[slice_number[0]: slice_number[1]]
        column_one.append(processed_data_one)
        column_two.append(processed_data_two)
    if len(columns) > 1:
        return column_one, column_two
    else:
        return column_one


def get_missing_value_percentage(data_resource):
    na_data = ((data_resource.isna().sum() / len(data_resource)) * 100).sort_values(ascending=False)
    missing_data = pd.DataFrame({'Missing %': na_data})
    print(missing_data)
    ax = sns.barplot(y=na_data.index, x=na_data)
    ax.set(xlabel='Percent of missing values', ylabel='Features', title='Percent missing data by columns')
    plt.show()


def get_iqr_boundary(data_resource, column):
    column_values = data_resource.loc[:, column]
    column_values_quantile = column_values.quantile([0.25, 0.5, 0.75, 1])
    column_values_iqr = column_values_quantile.iloc[2] - column_values_quantile.iloc[0]
    column_values_boundaries = [column_values_quantile.iloc[0] - (1.5 * column_values_iqr), column_values_quantile.iloc[1] + (1.5 * column_values_iqr)]
    return column_values_boundaries


def get_most_significant_columns(input_data, output_data, classifier, classifier_name):
    classifier.fit(input_data, output_data)
    feature_importances = pd.DataFrame(data=classifier.feature_importances_, index=input_data.columns, columns=['importance']).sort_values(by=['importance'], ascending=False)
    print(feature_importances)
    ax = sns.barplot(y=feature_importances.index, x=feature_importances.importance)
    ax.set(xlabel='Importance', ylabel='Column', title='The most important columns of ' + classifier_name)
    plt.show()


def get_train_data():
    data_resource = pd.read_csv(FILE_PATH + 'TrainingSet.csv')
    input_columns = np.r_[0, np.arange(1, 5, 1), np.arange(6, 11, 1), np.arange(12, 16, 1), 17, 18, np.arange(20, 34, 2), np.arange(34, 38, 1)]
    input_data = data_resource.iloc[:, input_columns]

    # get_missing_value_percentage(input_data)
    # if price == 0 or price.isna(), the qualified will be 99% 0, we cannot remove price column because it closly relates to qualified
    input_data = input_data.loc[input_data.loc[:, 'PRICE'].notna(), :]
    input_data = input_data.loc[input_data.loc[:, 'PRICE'] != 0, :]
    input_data = treat_missing_values(input_data)
    input_data = treat_special_values(input_data)
    input_data = treat_value_type(input_data, True)
    input_data = feature_binning(input_data)
    input_data = feature_crosses(input_data)

    output_data = data_resource.loc[:, ['PRICE', 'QUALIFIED']]
    output_data = output_data.loc[output_data.loc[:, 'PRICE'].notna(), ['PRICE', 'QUALIFIED']]
    output_data = output_data.loc[output_data.loc[:, 'PRICE'] != 0, 'QUALIFIED']
    # output_data_distribution = output_data.value_counts()
    # print(output_data_distribution)
    return input_data, output_data


def get_test_data():
    data_resource = pd.read_csv(FILE_PATH + 'TestingSet.csv')
    input_columns = np.r_[0, np.arange(1, 5, 1), np.arange(6, 11, 1), np.arange(12, 18, 1), np.arange(19, 33, 2), np.arange(33, 37, 1)]
    input_data = data_resource.iloc[:, input_columns]
    input_data = input_data.loc[input_data.loc[:, 'PRICE'].notna(), :]
    input_data = input_data.loc[input_data.loc[:, 'PRICE'] != 0, :]
    input_data = treat_missing_values(input_data)
    input_data = treat_special_values(input_data)
    input_data = treat_value_type(input_data, False)
    input_data = feature_binning(input_data)
    input_data = feature_crosses(input_data)
    return input_data


def treat_missing_values(data_resource):
    mode_columns = ['SSL', 'BATHRM', 'HF_BATHRM', 'HEAT', 'AC', 'NUM_UNITS', 'ROOMS', 'BEDRM', 'AYB', 'EYB', 'STORIES', 'SALEDATE', 'SALE_NUM', 'STYLE', 'STRUCT', 'GRADE', 'CNDTN', 'EXTWALL', 'ROOF', 'INTWALL', 'KITCHENS', 'FIREPLACES', 'USECODE']
    mean_columns = ['GBA', 'LANDAREA']
    for mode_column in mode_columns:
        data_resource.loc[:, mode_column].fillna(data_resource.loc[:, mode_column].value_counts().index[0], inplace=True)
    for mean_column in mean_columns:
        data_resource.loc[:, mean_column].fillna(data_resource.loc[:, mean_column].mean(), inplace=True)
    for no_zero_column in np.r_[mode_columns, mean_columns]:
        mode_value = data_resource.loc[:, no_zero_column].value_counts().index[0] if data_resource.loc[:, no_zero_column].value_counts().index[0] else data_resource.loc[:, no_zero_column].value_counts().index[1]
        data_resource.loc[data_resource.loc[:, no_zero_column] == 0, no_zero_column] = mode_value
    return data_resource


def treat_special_values(data_resource):
    data_resource['SQUARE'], data_resource['SUFFIX'] = value_split(data_resource, 'SSL', ['SQUARE', 'SUFFIX'], '\s+', False, False)
    data_resource.loc[:, 'SQUARE'] = preprocessor.LabelEncoder().fit_transform(data_resource.loc[:, 'SQUARE'].astype(str))
    data_resource.loc[:, 'SUFFIX'] = preprocessor.LabelEncoder().fit_transform(data_resource.loc[:, 'SUFFIX'].astype(str))
    data_resource = data_resource.drop(['SSL'], axis=1)

    data_resource.loc[:, 'AC'] = preprocessor.LabelEncoder().fit_transform(data_resource.loc[:, 'AC'].astype(str))
    data_resource.loc[:, 'SALEDATE'] = value_split(data_resource, 'SALEDATE', ['SALEDATE'], 'T', ('-', '.'), [0, 7])
    return data_resource


def treat_value_type(data_resource, is_training):
    possible_columns = ['row ID', 'PRICE', 'SALEDATE', 'SQUARE', 'CNDTN', 'GBA', 'EYB', 'AYB', 'AC', 'STRUCT', 'LANDAREA', 'GRADE']
    integer_columns = list_minus(possible_columns, ['SALEDATE'])
    training_columns = list_minus(possible_columns, ['row ID'])

    data_resource.loc[:, 'SALEDATE'] = data_resource.loc[:, 'SALEDATE'].astype(float)
    for integer_column in integer_columns:
        data_resource.loc[:, integer_column] = data_resource.loc[:, integer_column].astype(int)
    if is_training:
        data_resource = data_resource.loc[:, training_columns]
    else:
        data_resource = data_resource.loc[:, possible_columns]
    return data_resource


def feature_binning(data_resource):
    binning_columns = [('PRICE', 10), ('SALEDATE', 5), ('GBA', 7), ('EYB', 5), ('AYB', 5)]
    # binning_columns = [('PRICE', 10)]
    for column_tuple in binning_columns:
        column_values = data_resource.loc[:, column_tuple[0]]
        column_min = column_values.min()
        column_max = column_values.max()
        bin_interval = (column_max - column_min) / column_tuple[1]
        bin_boundaries = np.arange(column_min, column_max + 1, bin_interval)
        data_resource['BINNING_' + column_tuple[0]] = pd.cut(column_values, bin_boundaries, labels=np.arange(1, len(bin_boundaries), 1))
        data_resource['BINNING_' + column_tuple[0]].fillna(1, inplace=True)
    return data_resource


def feature_crosses(data_resource):
    divide_columns = [('PRICE', 'GBA'), ('PRICE', 'LANDAREA'), ('PRICE', 'GRADE')]
    minus_columns = [('SALEDATE', 'AYB'), ('SALEDATE', 'EYB'), ('EYB', 'AYB'), ('GBA', 'LANDAREA')]
    for divide_tuple in divide_columns:
        data_resource[divide_tuple[0] + '_DIVIDE_' + divide_tuple[1]] = data_resource.loc[:, divide_tuple[0]] / data_resource.loc[:, divide_tuple[1]]
    for minus_tuple in minus_columns:
        data_resource[minus_tuple[0] + '_MINUS_' + minus_tuple[1]] = data_resource.loc[:, minus_tuple[0]] - data_resource.loc[:, minus_tuple[1]]
    return data_resource


def plot_confusion_matrix(cm):
    plt.imshow(cm, cmap=plt.cm.Blues)
    plt.title('Confusion matrix')
    plt.xticks([0, 1], [0, 1], rotation=0)
    plt.yticks([0, 1], [0, 1])
    thresh = cm.max() / 2
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, cm[i, j], horizontalalignment="center", color='white' if cm[i, j] > thresh else 'black')
    plt.ylabel('True Classes')
    plt.xlabel('Predicted Classes')
    plt.show()


def plot_roc(test_output, predict_test_output):
    fpr, tpr, thresholds = roc_curve(test_output, predict_test_output)
    roc_auc = auc(fpr, tpr)
    plt.title('Receiver Operating Characteristic')
    plt.plot(fpr, tpr, 'b', label='AUC = %0.3f' % roc_auc)
    plt.legend(loc='lower right')
    plt.plot([0, 1], [0, 1], 'r--')
    plt.xlim([-0.1, 1.0])
    plt.ylim([-0.1, 1.01])
    plt.ylabel('True Positive Rate')
    plt.xlabel('False Positive Rate')
    plt.show()


def write_data(row_prediction, algorithm):
    data_resource_rows = pd.read_csv(FILE_PATH + 'TestingSet.csv').loc[:, 'row ID']
    zero_row = list_minus(data_resource_rows, row_prediction.loc[:, 'row ID'])
    zero_dataframe = pd.DataFrame({'row ID': zero_row, 'QUALIFIED': 0})
    data_resource = pd.concat([row_prediction, zero_dataframe]).sort_index()
    data_resource.to_csv(FILE_PATH + algorithm + '\\TestingSet-Random.csv', index=False)
