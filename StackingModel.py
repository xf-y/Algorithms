from mlxtend.classifier import EnsembleVoteClassifier
from sklearn.model_selection import train_test_split
from imblearn.over_sampling import SMOTE
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
from tensorflow.keras import Sequential
from tensorflow.keras.layers import Dense
import AssignmentThreeTools as a3Tools
import pandas as pd
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier


data_input, data_output = a3Tools.get_train_data()

clf1 = RandomForestClassifier(n_estimators=800, criterion='entropy', class_weight={1: 1.5, 0: 12})
clf2 = GradientBoostingClassifier(n_estimators=860, warm_start=True)
clf3 = LogisticRegression()

clf4 = Sequential()
clf4.add(Dense(20, input_dim=len(data_input.columns), activation='relu'))
clf4.add(Dense(len(data_input.columns), activation='relu'))
clf4.add(Dense(1, activation='sigmoid'))
clf4.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

sclf = EnsembleVoteClassifier(clfs=[clf1, clf2, clf3, clf4], weights=[2, 4, 1, 1], voting='soft')

while True:
    train_input, test_input, train_output, test_output = train_test_split(data_input, data_output, train_size=0.8, test_size=0.2)
    train_input_smote, train_output_smote = SMOTE(random_state=2).fit_sample(train_input, train_output)
    sclf.fit(train_input_smote, train_output_smote)
    predict_test_output = sclf.predict(test_input)
    predict_accuracy = accuracy_score(test_output, predict_test_output)
    if predict_accuracy > 0.867:
        data_test = a3Tools.get_test_data()
        data_input_rowid = data_test.loc[:, 'row ID']
        data_input_test = data_test.iloc[:, 1:]
        data_output_test = sclf.predict(data_input_test)
        a3Tools.write_data(pd.DataFrame({'row ID': data_input_rowid, 'QUALIFIED': data_output_test}), 'GradientBoosting')
        break
    else:
        print(predict_accuracy)
