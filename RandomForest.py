import AssignmentThreeTools as a3Tools
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import GridSearchCV, train_test_split, cross_val_score
from sklearn.metrics import confusion_matrix, accuracy_score
from sklearn.ensemble import AdaBoostClassifier
from imblearn.over_sampling import SMOTE
import numpy as np
import pandas as pd


# 'max_depth': np.arange(1, 10, 1),
# 'min_samples_leaf': np.arange(1, 10, 1),
# 'n_estimators': np.arange(50, 100, 10),
# 'class_weight': [{0: 1}, {0: 1.1}, {0: 1.3}, {0: 1.4}]
def optimize_forest_parameter(input_data, output_data):
    forest_tree_param = {
        'max_depth': np.arange(1, 10, 1),
        'min_samples_leaf': np.arange(1, 10, 1),
        'n_estimators': np.arange(50, 100, 10),
        'class_weight': [{0: 1}, {0: 1.1}, {0: 1.3}, {0: 1.4}]
    }
    optimized_classifier = GridSearchCV(RandomForestClassifier(), forest_tree_param, scoring='f1', cv=5)
    optimized_classifier.fit(input_data, output_data)
    print(optimized_classifier.best_params_, optimized_classifier.best_score_)


data_input, data_output = a3Tools.get_train_data()
train_input, test_input, train_output, test_output = train_test_split(data_input, data_output, train_size=0.5, test_size=0.5)
train_input_smote, train_output_smote = SMOTE(random_state=2).fit_sample(train_input, train_output)
# print(sum(train_output_smote == 1))
# print(sum(train_output_smote == 0))

# optimize_forest_parameter(train_input, train_output)
random_forest_classifier = RandomForestClassifier(n_estimators=800, criterion='entropy', class_weight={1: 1.5, 0: 12})
random_forest_classifier.fit(train_input, train_output)
# predict_test_output = random_forest_classifier.predict(test_input)
# print(accuracy_score(test_output, predict_test_output))
predict_test_output = a3Tools.threshold_moving(random_forest_classifier.predict_proba(test_input), 0.51)
print(accuracy_score(test_output, predict_test_output))

a3Tools.plot_confusion_matrix(confusion_matrix(test_output, predict_test_output))
a3Tools.plot_roc(test_output, predict_test_output)

# check to avoid over-fitting
validate_score = cross_val_score(random_forest_classifier, train_input, train_output, cv=50, scoring='accuracy')
print(validate_score.mean())

data_test = a3Tools.get_test_data()
data_input_rowid = data_test.loc[:, 'row ID']
data_input_test = data_test.iloc[:, 1:]
data_output_test = random_forest_classifier.predict(data_input_test)
a3Tools.write_data(pd.DataFrame({'row ID': data_input_rowid, 'QUALIFIED': data_output_test}), 'RandomForest')
