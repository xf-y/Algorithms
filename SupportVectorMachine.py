import AssignmentThreeTools as a3Tools
import sklearn.svm as svm
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.model_selection import GridSearchCV
import numpy as np
import pandas as pd


def optimize_svm_parameter():
    grid = GridSearchCV(svm.SVC(kernel='rbf'), param_grid={"C": np.arange(1, 20, 1), "gamma": [0.000001, 0.0000001, 0.00000001, 0.000000001, 0.0000000001]}, cv=5, scoring='f1', n_jobs=4)
    grid.fit(train_input, train_output)
    print("The best parameters are %s with a score of %0.2f" % (grid.best_params_, grid.best_score_))


data_input, data_output = a3Tools.get_train_data()
train_input, test_input, train_output, test_output = train_test_split(data_input, data_output, train_size=0.8, test_size=0.2)
svm_classifier = svm.SVC(kernel='rbf', gamma=0.01, C=15)
svm_classifier.fit(train_input, train_output)
predict_validate = svm_classifier.predict(test_input)
print(accuracy_score(test_output, predict_validate))

data_test = a3Tools.get_test_data()
data_input_rowid = data_test.loc[:, 'row ID']
data_input_test = data_test.iloc[:, 1:]
data_output_test = svm_classifier.predict(data_input_test)
a3Tools.write_data(pd.DataFrame({'row ID': data_input_rowid, 'QUALIFIED': data_output_test}), 'SupportVectorMachine')
