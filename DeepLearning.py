from tensorflow.keras import Sequential
from tensorflow.keras.layers import Dense
from sklearn.model_selection import train_test_split
import AssignmentThreeTools as a3Tools


data_input, data_output = a3Tools.get_train_data()
train_input, test_input, train_output, test_output = train_test_split(data_input, data_output, train_size=0.7, test_size=0.3)

dnn_classifier = Sequential()
dnn_classifier.add(Dense(20, input_dim=len(data_input.columns), activation='relu'))
dnn_classifier.add(Dense(len(data_input.columns), activation='relu'))
dnn_classifier.add(Dense(1, activation='sigmoid'))
dnn_classifier.compile(loss='binary_crossentropy', optimizer='adam', metrics=['f1'])
dnn_classifier.fit(train_input, train_output, batch_size=1, nb_epoch=10)
loss, accuracy = dnn_classifier.evaluate(test_input, test_output)
print(accuracy)
