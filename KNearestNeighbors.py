import AssignmentThreeTools as a3Tools
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import cross_val_score
from sklearn.ensemble import BaggingClassifier, RandomForestClassifier, GradientBoostingClassifier
from sklearn.model_selection import GridSearchCV
from imblearn.over_sampling import SMOTE
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


def get_k_value(input_data, output_data):
    k_value_score = pd.DataFrame(columns=['K_Value', 'Score'])
    for k in np.arange(1, 100, 1):
        knn_classifier_k_value = KNeighborsClassifier(n_neighbors=k)
        scores = cross_val_score(knn_classifier_k_value, input_data, output_data, cv=5, scoring='f1')
        k_value_score = k_value_score.append({'K_Value': k, 'Score': scores.mean()}, ignore_index=True)
    print(k_value_score)
    plt.plot(k_value_score['K_Value'], k_value_score['Score'])
    plt.xlabel('Value of K for KNearestNeighbors')
    plt.ylabel('Cross-Validated F1')
    plt.show()


def optimize_bagging_patameter(input_data, output_data):
    bagging_param = {
        'n_estimators': np.arange(50, 200, 10)
    }
    optimized_classifier = GridSearchCV(BaggingClassifier(KNeighborsClassifier(n_neighbors=1)), bagging_param, n_jobs=4, cv=5, scoring='f1')
    optimized_classifier.fit(input_data, output_data)
    print(optimized_classifier.best_params_, optimized_classifier.best_score_)


data_input, data_output = a3Tools.get_train_data()
train_input, test_input, train_output, test_output = train_test_split(data_input, data_output, train_size=0.7, test_size=0.3)
train_input_smote, train_output_smote = SMOTE(random_state=2).fit_sample(train_input, train_output)

# a3Tools.get_most_significant_columns(data_input, data_output, RandomForestClassifier(), 'RandomForest')
# a3Tools.get_most_significant_columns(data_input, data_output, GradientBoostingClassifier(), 'GradientBoosting')
get_k_value(train_input_smote, train_output_smote)
optimize_bagging_patameter(train_input_smote, train_output_smote)

# f1:1, accuracy: 148
knn_classifier = BaggingClassifier(KNeighborsClassifier(n_neighbors=148), n_estimators=100, n_jobs=4)
knn_classifier.fit(train_input, train_output)
print(knn_classifier.score(test_input, test_output))

# check to avoid over-fitting
validate_score = cross_val_score(knn_classifier, train_input, train_output, cv=50, scoring='f1')
print(validate_score.mean())

data_test = a3Tools.get_test_data()
data_input_rowid = data_test.loc[:, 'row ID']
data_input_test = data_test.iloc[:, 1:]
data_output_test = knn_classifier.predict(data_input_test)
a3Tools.write_data(pd.DataFrame({'row ID': data_input_rowid, 'QUALIFIED': data_output_test}), 'KNearestNeighbors')
